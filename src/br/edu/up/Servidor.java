package br.edu.up;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor implements Runnable {

  public static void main(String[] args) {
    new Thread(new Servidor()).run();
  }

  @Override
  public void run() {
    
    int contador = 1;
    ServerSocket serverSocket = null;
    Socket socket = null;
    DataOutputStream dos = null;
    DataInputStream dis = null;

    while (true) {
      try {
        
        String mensagem = "Servidor: " + contador;
        System.out.println("Servidor aguardando conex�o!");
        serverSocket = new ServerSocket(8888);
        socket = serverSocket.accept();
        dos = new DataOutputStream(socket.getOutputStream());
        dis = new DataInputStream(socket.getInputStream());
        dos.writeUTF(mensagem);
        System.out.println("Cliente: " + dis.readUTF());
        contador++;
        //192.168.137.1
      } catch (Exception e) {
        
        System.out.println("Ping!!!!!");
        //e.printStackTrace();
      
      } finally {

        if (dos != null) {
          try {
            dos.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (dis != null) {
          try {
            dis.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (socket != null) {
          try {
            socket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (serverSocket != null) {
          try {
            serverSocket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    }
  }
}